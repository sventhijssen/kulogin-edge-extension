# KULogin Edge Extension

## How to install

Clone our repository. Fill out your username and password in extension > js > background.js. Open Microsoft Edge. Import the cloned folder "extension" at Edge menu > Extensions > Load Extension. You're set.

## How to use
Add the extension to your tabs. In order to sign in, click "Sign in" anytime and you will be redirected to your Toledo dashboard.

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

* [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.txt)
* [License](license.txt)